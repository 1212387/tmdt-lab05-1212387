﻿using MVCWebAPIClient1212387.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MVCWebAPIClient1212387.Controllers
{
    public class UsersController : Controller
    {
        //
        // GET: /Users/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult getAllUser1212387()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4083/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // New code:
                HttpResponseMessage response = client.GetAsync("api/2.0/users").Result;
                if (response.IsSuccessStatusCode)
                {
                    var _users = response.Content.ReadAsAsync<List<Users>>().Result;
                    ViewData["users"] = _users;
                }
            }
            return View("getAllUser");
        }



        [HttpPost]
        public ActionResult getAllUserByUsername1212387(Users model)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4083/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // New code:
                HttpResponseMessage response = client.GetAsync("api/2.0/users/" + model.username).Result;
                if (response.IsSuccessStatusCode)
                {
                    var _users = response.Content.ReadAsAsync<Users>().Result;
                    ViewData["user"] = _users;
                }

            }
            return View("getAllUserByUsername");
        }


        [HttpPost]
        public ActionResult addUser1212387(Users model)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4083/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                var temp = new Users() { username = model.username, password = model.password };

                // New code:
                HttpResponseMessage response = client.PostAsJsonAsync("api/2.0/users", temp).Result;
                
            }
            return RedirectToAction("getAllUser1212387");
        }


        [HttpPost]
        public ActionResult deleteUser1212387(Users model)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4083/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                

                // New code:
                HttpResponseMessage response = client.DeleteAsync("api/2.0/users/" +model.username).Result;
            }
            return RedirectToAction("getAllUser1212387");
        }


        [HttpPost]
        public ActionResult updateUser1212387(Users model)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4083/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));



                var temp = new Users() { username = model.username, password = model.confirmpassword };
                // New code:
                HttpResponseMessage response = client.PutAsJsonAsync("api/2.0/users/" + model.username, temp).Result;
            }
            return RedirectToAction("getAllUser1212387");
        }


      
    }
}