﻿using MVCWebAPIClient1212387.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MVCWebAPIClient1212387.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult searchCategory1212387(string category)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4083/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // New code:
                HttpResponseMessage response = client.GetAsync("api/2.0/products?category=" + category).Result;
                if (response.IsSuccessStatusCode)
                {
                    var products = response.Content.ReadAsAsync<List<Product>>().Result;
                    ViewData["products"] = products;
                }
            }
            return View("getProducts");
        }
	}
}