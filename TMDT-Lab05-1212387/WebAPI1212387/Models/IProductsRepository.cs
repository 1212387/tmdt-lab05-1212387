﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI1212387.Models
{
    interface IProductsRepository
    {

        IEnumerable<Products> getProductByCategory1212387(string category);

        IEnumerable<Products> getProductByCategoryAndUsername1212387(string category, string username);

    }
}
