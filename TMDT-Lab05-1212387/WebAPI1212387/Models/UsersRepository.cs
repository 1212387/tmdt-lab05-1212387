﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212387.Models
{
    public class UsersRepository:IUsersRepository
    {
        private List<Users> users = new List<Users>();

        public UsersRepository()
        {
            users.Add(new Users { username = "1212387", password = "123456" });
            users.Add(new Users { username = "user1", password = "123456" });
            users.Add(new Users { username = "user2", password = "123456" });
        }

        public IEnumerable<Users> getAllUser1212387()
        {
            return users;
        }

        public Users getUserByUsername1212387(string username)
        {
            return users.Find(p => p.username == username);
        }

        public bool addUser1212387(Users newUser)
        {
            if (users.Find(p => p.username == newUser.username) != null)
            {
                return false;
            }
            users.Add(newUser);
            return true;
        }

        public bool deleteUserByUsername1212387(string username)
        {
            if (users.Find(p => p.username == username) == null)
            {
                return false;
            }
            users.RemoveAll(p => p.username == username);
            return true;
        }

        public bool updateUserByUsername1212387(string username, Users updateUser)
        {
            int index = users.FindIndex(p => p.username == username);
            if (index == -1)
            {
                return false;
            }
            users.RemoveAt(index);
            users.Add(updateUser);
            return true;
        }
    }
}