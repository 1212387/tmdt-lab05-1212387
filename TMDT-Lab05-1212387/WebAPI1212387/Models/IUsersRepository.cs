﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI1212387.Models
{
    interface IUsersRepository
    {
        IEnumerable<Users> getAllUser1212387();
        Users getUserByUsername1212387(string username);
        bool addUser1212387(Users newUser);
        bool deleteUserByUsername1212387(string username);
        bool updateUserByUsername1212387(string username, Users updateUser);
    }
}
