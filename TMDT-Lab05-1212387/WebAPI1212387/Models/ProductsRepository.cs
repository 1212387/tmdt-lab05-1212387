﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212387.Models
{
    public class ProductsRepository : IProductsRepository
    {
        private List<Products> products = new List<Products>();
        private int _nextId = 1;

        public ProductsRepository()
        {
            products.Add(new Products { Name = "Tomato soup", Category = "Groceries", Price = "123", username = "1212387" });
            products.Add(new Products { Name = "Yo-yo", Category = "Toys", Price = "234", username = "user1" });
            products.Add(new Products { Name = "Hammer", Category = "Hardware", Price = "345", username = "user2" });
        }

       

        public IEnumerable<Products> getProductByCategory1212387(string category)
        {
            return products.Where(p => string.Equals(p.Category, category, StringComparison.OrdinalIgnoreCase));
        }

        public IEnumerable<Products> getProductByCategoryAndUsername1212387(string category, string username)
        {
            return products.Where(p => string.Equals(p.Category, category, StringComparison.OrdinalIgnoreCase) && string.Equals(p.username, username, StringComparison.OrdinalIgnoreCase));
        }
    }
}