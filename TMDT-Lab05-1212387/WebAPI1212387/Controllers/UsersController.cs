﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212387.Models;

namespace WebAPI1212387.Controllers
{
    public class UsersController : ApiController
    {
        static readonly IUsersRepository repository = new UsersRepository();

      
        [HttpGet]
        [Route("api/2.0/users")]
        public IHttpActionResult getAllUser1212387()
        {
            return Ok(repository.getAllUser1212387());
        }

        [HttpGet]
        [Route("api/2.0/users/{username}")]
        public IHttpActionResult getUserByUsername1212387([FromUri] string username)
        {
            
            return Ok(repository.getUserByUsername1212387(username));
        }

        [HttpPost]
        [Route("api/2.0/users")]
        public HttpResponseMessage addUser1212387([FromBody] Users newuser)
        {
            if(!repository.addUser1212387(newuser))
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            return Request.CreateResponse(HttpStatusCode.Created);
        }


        [HttpDelete]
        [Route("api/2.0/users/{username}")]
        public IHttpActionResult deleteUserByUsername1212387([FromUri] string username)
        {
            if (!repository.deleteUserByUsername1212387(username))
                return NotFound();
            
            return Ok();
        }

        [HttpPut]
        [Route("api/2.0/users/{username}")]
        public IHttpActionResult updateUserByUsername1212387([FromUri] string username, [FromBody] Users updateUser)
        {
            if (!repository.updateUserByUsername1212387(username, updateUser))
                return NotFound();
            return Ok();
        }

    }
}
