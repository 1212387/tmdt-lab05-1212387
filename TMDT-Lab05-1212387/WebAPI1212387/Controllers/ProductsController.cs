﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212387.Models;

namespace WebAPI1212387.Controllers
{
    public class ProductsController : ApiController
    {
        static readonly IProductsRepository repository = new ProductsRepository();

        [HttpGet]
        [Route("api/2.0/products")]
        public IHttpActionResult getProducts1212387([FromUri] string category)
        {
            return Ok(repository.getProductByCategory1212387(category));
        }

        [HttpGet]
        [Route("api/2.0/products/{category}/{username}")]
        public IHttpActionResult getProducts1212387([FromUri] string category, [FromUri] string username)
        {
            return Ok(repository.getProductByCategoryAndUsername1212387(category,username));
        }

    }
}
